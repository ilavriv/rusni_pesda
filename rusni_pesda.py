from concurrent.futures import ThreadPoolExecutor

import requests
from prettytable import PrettyTable

BASE_URL = "https://uadata.net/ukraine-russia-war-2022/{}.json"
DOCUMENTS = [
    "bbm",
    "planes",
    "tanks",
    "artilery",
    "rszv",
    "ppo",
    "helicopters",
    "auto",
    "bpla",
    "people",
]


def fetch_rusnia(url):
    response = requests.get(url)
    json = response.json()

    return json["data"][0]["val"]


def get_table(data):
    capitalize = lambda t: t.capitalize()
    titles = list(map(capitalize, DOCUMENTS))
    table = PrettyTable()

    table.field_names = titles
    table.add_row(data)
    return table


def fetch():
    with ThreadPoolExecutor() as executor:
        urls = [BASE_URL.format(d) for d in DOCUMENTS]

        return list(executor.map(fetch_rusnia, urls))


def main():
    data = fetch()
    table = get_table(data)

    print(table)


async def wsgi_app(scope, receive, send):
    assert scope["type"] == "http"

    data = fetch()
    table = get_table(data)

    await send(
        {
            "type": "http.response.start",
            "status": 200,
            "headers": [
                [b"content-type", b"text/plain"],
            ],
        }
    )

    await send(
        {
            "type": "http.response.body",
            "body": str(table).encode("utf-8"),
        }
    )


if __name__ == "__main__":
    main()
